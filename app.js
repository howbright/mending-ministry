const express = require('express');
const session =require('express-session')
const bodyParser = require('body-parser');
const logger = require('morgan')
const cors = require('cors')
const helmet = require('helmet')
const models= require('./models')
const auth = require('./auth')
app = express()

app.use(helmet({
  frameguard: false
}));
app.use(cors())
app.use(logger('dev'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}))

app.use('/user', require('./api/user'))
app.use('/letter', require('./api/letter'))
app.use('/statistic', require('./api/statistic'))





let users = [
  {
  	id: 1,
  	name: 'alice'
  },
  {
  	id: 2,
  	name: 'bek'
  },
  {
  	id: 3,
  	name: 'chris'
  }
]

// app.get('/users', (req, res) => res.json(users));

// app.get('/users/:id', (req, res) => {
// 	const id = parseInt(req.params.id, 10);
// 	if(!id){
// 		return res.status(400).json({error: 'Incorrect id'});
// 	}
// 	let user = users.filter(user => user.id === id)[0];
// 	if(!user){
// 		return res.status(404).json({error: 'Unknown user'});
// 	}
// 	return res.json(user);
// });

// app.delete('/users/:id', (req, res) => {

// 	const id = parseInt(req.params.id, 10);
// 	if(!id){
// 		return res.status(400).json({error: 'Incorrect id'});
// 	}
// 	const userIdx = users.findIndex(user => user.id === id);
// 	if(userIdx === -1){
// 		return res.status(404).json({error: 'Unknown user'});
// 	}

// 	users.splice(userIdx, 1);
// 	res.status(204).send();
// });
app.post('/users', (req, res) => {
	// console.log(req)
	// console.log(req.body)
	const name = req.body.name || '';
	if(!name.length){
		return res.status(400).json({error: 'Incorrect name'});
	}

	// // 새로운 Id 만들기 
	// const id = users.reduce((maxId, user) => maxId > user.id ? maxId : user.id, 0) + 1;

	// const newUser = {
	// 	id: id,
	// 	name: name
	// }
	// users.push(newUser);
	return res.status(201).json({mssage:'good'});

});


http = require('http'),

httpServer = http.Server(app);
var history = require('connect-history-api-fallback');
// // /home/ubuntu/nodejs/dist/spa-mat
// // /Users/hyun/Projects/www/dcms-bot/dist/spa-mat
app.use(history())
app.use(express.static('./dist/spa-mat'))
// app.get('/', function(req, res) {
//   res.sendfile('./dist/spa-mat/index.html')
// });
app.listen(8082, () => {
  console.log('Example app listening on port 8082!');

    models.sequelize.sync({force: false})
      .then(() => {
        console.log('Databases sync');
      }).catch((err) => console.log(err));
});





