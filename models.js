const Sequelize = require('sequelize');
const sequelize = new Sequelize('mending-ministry', 'root', 'moseswife!@',{
	host: 'ec2-3-34-137-148.ap-northeast-2.compute.amazonaws.com',
  dialect: 'mysql',
  operatorsAliases: false
});

const User = sequelize.define('user', {
  user_id: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: true
  },
  pwd: {
    type: Sequelize.STRING
  },
  gender: {
    type: Sequelize.INTEGER
  },
  born_year: {
    type: Sequelize.STRING
  }
});

const Letter = sequelize.define('letter', {
  letter_id: {
    type: Sequelize.INTEGER,
    primaryKey: true, 
    autoIncrement: true
  },
  title: Sequelize.STRING,
  subtitle: Sequelize.STRING,
  intro_msg: Sequelize.STRING,
  test: Sequelize.STRING,
  //0: 비공개 // 1: 공개 대기중// 2: 공개 
  status: {
    type: Sequelize.TINYINT,
    defaultValue: 0
  },
  content: Sequelize.TEXT('medium'),
  open_count: Sequelize.INTEGER,
  love_count: Sequelize.INTEGER,
  share_count: Sequelize.INTEGER,
  video_id: Sequelize.STRING,
  user_id: {
    type: Sequelize.STRING,
    references: {
      model: User,
      key: 'user_id'
    }
  },
});

const Statistic = sequelize.define('statistic', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  facebook: {
    type: Sequelize.INTEGER
  },
  favorite: {
    type: Sequelize.INTEGER
  }
});

const Open = sequelize.define('open', {
  letter_id: {
  	type: Sequelize.INTEGER,
  	primaryKey: true,
  	references: {
     model: Letter,
     key: 'letter_id'
    }
  },
  ip: {
    type: Sequelize.STRING,
    primaryKey: true
  }
});


module.exports = {
  sequelize: sequelize,
  User: User, 
  Letter: Letter,
  Statistic: Statistic,
  Open: Open
}

