const models = require('../../models')
const auth = require('../../auth')

let letters = [
  {
    id: 1,
    name: 'alice',
    open: 0,
    love: 0,
    share: 0
  },
  {
    id: 2,
    name: 'bek',
    open: 0,
    love: 0,
    share: 0
  },
  {
    id: 3,
    name: 'chris',
    open: 0,
    love: 0,
    share: 0
  }
]




exports.show = (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(!id) {
  	return res.status(400).json({error: 'Incorrect id'});
  }

  models.Letter.findOne({
  	where: {
  		id: id
  	}
  }).then(letter => {
  	if(!letter){
  		return res.status(404).json({error: 'No Letter'});
  	}
      return res.status(200).json(letter);
  })
};

exports.increaseOpen = (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(!id) {
    return res.status(400).json({error: 'Incorrect id'});
  }
  models.Letter.findById(id).then(letter => {
    return letter.increment('open', {by: 1})
  }).then(letter => {
  	return res.status(200).json(letter);
  // Postgres will return the updated user by default (unless disabled by setting { returning: false })
  // In other dialects, you'll want to call user.reload() to get the updated instance...
  });
}
exports.checkOpen = (req, res) => {
  models.Open.create({letter_id: 1, ip: 789 }).then(function(result) {
  	  models.Letter.findById(1).then(letter => {
    return letter.increment('open', {by: 1})
  }).then(letter => {
  	return res.status(200).json(letter);
  // Postgres will return the updated user by default (unless disabled by setting { returning: false })
  // In other dialects, you'll want to call user.reload() to get the updated instance...
  });
   // return res.status(200).json(result);
}).catch(function(err) {
    return res.status(404).json({error: 'No Letter'});
});
}

exports.destroy = (req, res) => {

};



//편지 제목 생성 
mNames = ['요셉','베드로','이삭','야고보','요한','다윗','스데반','바울','바나바','마가','누가','마태','디모데']
wNames = ['라헬','룻','마리아','한나', '사라', '아비가일']

function getRandomIntInclusive(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getManName(){
  return mNames[getRandomIntInclusive(0,12)]
}
function getWomanName() {
  return wNames[getRandomIntInclusive(0,5)]
}
exports.create = (req, res) => {
  console.log('진입')
  models.User.findOne({
    where: {
      user_id: req.body.user_id
    }
  }).then(user => {
    console.log(user.dataValues)
    let age = 2018 - user.dataValues['born_year']
    console.log('age: ' + age)
    let gender = user.dataValues['gender']
    console.log('gender: ' + gender)
    let name = gender === 0 ? getManName() : getWomanName()
    console.log('name: ' + name)
    let title = age + '세, ' + name + '의 이야기'
    let body = {
      title : title,
      content: req.body.content,
      subtitle: req.body.subtitle,
      user_id: req.body.user_id
    }
    models.Letter.create(body).then(function(result){
      console.log('편지생성 성공')
      return res.status(200).json(result)
    }).catch(function(err){ 
      return res.status(404).json({error: 'letter register fail'})
    })
  }).catch(function(err){
    return res.status(404).json({error: '제목에러'})
  })
}
// `letter_id`, `title`, `subtitle`, `intro_msg`, `content`, `open_count`, `love_count`, `share_count`, `user_id`
exports.showUserLetters = (req, res) => {
  let user
  try {
    user = auth.verify(req.headers.authorization)
  } catch (e) {
    console.log(e)
  }
  console.log(user)
  if(!user || req.params.id !== user.id){
    return res.status(401).json({error: '권한없음'})
  }
  return models.Letter.findAll({
    attributes: ['letter_id', 'title', 'subtitle', 'intro_msg', 'open_count', 'love_count', 'share_count', 'video_id', 'user_id', 'status'],
    where: {
      user_id: req.params.id
    }
  }).then(letters => res.status(200).json(letters)).catch(function(err){
    return res.status(404).json({error: '에러에러'})
  });
};


exports.showAdminLetters = (req, res) => {
  let user
  try {
    user = auth.verify(req.headers.authorization)
  } catch (e) {
    console.log(e)
  }
  if(!user || 'admin' !== user.id){
    return res.status(401).json({error: '권한없음'})
  }
  return models.Letter.findAll({
    attributes: ['letter_id', 'title', 'subtitle', 'intro_msg', 'open_count', 'love_count', 'share_count', 'user_id', 'status'],
    where: {
      status: [1,2]
    }
  }).then(letters => res.status(200).json(letters)).catch(function(err){
    return res.status(404).json({error: '에러에러'})
  });
};

exports.showAllLetters = (req, res) => {
  return models.Letter.findAll({
    attributes: ['letter_id', 'title', 'subtitle', 'intro_msg', 'open_count', 'love_count', 'share_count', 'video_id' ,'user_id', 'status'],
    where: {
      status: 2
    }
  }).then(letters => res.status(200).json(letters)).catch(function(err){
    return res.status(404).json({error: '에러에러'})
  });
};

exports.showLetter = (req, res) => {
  return models.Letter.findOne({
    where: {
      letter_id: req.params.letter_id
    }
  }).then(letter => 
    res.status(200).json(letter)).catch(function(err){
    return res.status(404).json({error:'에러~~'})
  });
}

exports.editLetter = (req,res) => {
   models.Letter.update(req.body,{
    where: {
      letter_id: req.params.letter_id
    }
   }).then(function(result){
      console.log('편지수정 성공')
      console.log(result)
      return res.status(200).json(result)
    }).catch(function(err){ 
      return res.status(404).json({error: 'letter edit fail'})
    })
}


//여기서부터 실구현 
// exports.create = (req, res) => {
//   let title = makeTitle(req.body.user_id)
//   console.log(title)
//   let body = {
//     title : title,
//     content: req.body.content,
//     subtitle: req.body.subtitle,
//     user_id: req.body.user_id
//   }
//   console.log(body)
//   models.Letter.create(body).then(function(result){
//     console.log('편지생성 성공')
//     return res.status(200).json(result)
//   }).catch(function(err){ 
//     return res.status(404).json({error: 'letter register fail'})
//   })
// };