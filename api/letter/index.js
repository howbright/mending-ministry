const express = require('express');
const router = express.Router();
const controller = require('./letter.controller');

router.post('/', controller.create);
router.get('/user/:id', controller.showUserLetters);
router.get('/admin', controller.showAdminLetters);
router.get('/all', controller.showAllLetters);
router.get('/:letter_id', controller.showLetter);
router.put('/:letter_id', controller.editLetter);
router.get('/:id/open', controller.increaseOpen);
router.get('/:id/check-open', controller.checkOpen);


module.exports = router;