const express = require('express');
const router = express.Router();
const auth = require('../../auth');
const controller = require('./user.controller');

router.post('/', controller.create);
router.get('/:id', controller.show);
// router.post('/:id', controller.show);
// router.get('/:id/open', controller.increaseOpen);
// router.get('/:id/check-open', controller.checkOpen);
//인증 테스트
router.get('/auth/home', controller.auth_home)
router.get('/auth/me', auth.ensureAuth(), controller.auth_me)
router.post('/login', controller.login)

module.exports = router;