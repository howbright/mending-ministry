const models = require('../../models')
const auth = require('../../auth')

let letters = [
  {
    id: 1,
    name: 'alice',
    open: 0,
    love: 0,
    share: 0
  },
  {
    id: 2,
    name: 'bek',
    open: 0,
    love: 0,
    share: 0
  },
  {
    id: 3,
    name: 'chris',
    open: 0,
    love: 0,
    share: 0
  }
]


exports.index = (req, res) => {
  return models.Letter.findAll().then(letters => res.json(letters));
};

exports.show = (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(!id) {
  	return res.status(400).json({error: 'Incorrect id'});
  }

  models.Letter.findOne({
  	where: {
  		id: id
  	}
  }).then(letter => {
  	if(!letter){
  		return res.status(404).json({error: 'No Letter'});
  	}
      return res.status(200).json(letter);
  })
};

exports.increaseOpen = (req, res) => {
  const id = parseInt(req.params.id, 10);
  if(!id) {
    return res.status(400).json({error: 'Incorrect id'});
  }
  models.Letter.findById(id).then(letter => {
    return letter.increment('open', {by: 1})
  }).then(letter => {
  	return res.status(200).json(letter);
  // Postgres will return the updated user by default (unless disabled by setting { returning: false })
  // In other dialects, you'll want to call user.reload() to get the updated instance...
  });
}
exports.checkOpen = (req, res) => {
  models.Open.create({letter_id: 1, ip: 789 }).then(function(result) {
  	  models.Letter.findById(1).then(letter => {
    return letter.increment('open', {by: 1})
  }).then(letter => {
  	return res.status(200).json(letter);
  // Postgres will return the updated user by default (unless disabled by setting { returning: false })
  // In other dialects, you'll want to call user.reload() to get the updated instance...
  });
   // return res.status(200).json(result);
}).catch(function(err) {
    return res.status(404).json({error: 'No Letter'});
});
}

exports.destroy = (req, res) => {

};

// 여기서부터 실구현 
exports.create = (req, res) => {
  const pwd2 = req.body.pwd2
  if (pwd2 !== 'phil0106') {
    return res.status(401).json({error: 'Login failure'})
  }
  console.log(req.body)
  models.User.create(req.body).then(function(result){
    console.log(result)
    return res.status(200).json(result)
  }).catch(function(err){ 
    return res.status(404).json({error: 'user register fail'})
  })
};

exports.show = (req, res) => {
  const id = req.params.id;
  const pwd = req.query.pwd;
  if(!id) {
    return res.status(400).json({error: 'Incorrect id'});
  }
  models.User.findOne({
    where: {
      user_id: id,
      pwd: pwd
    }
  }).then(user => {
    console.log('result: ' + user)
    if(!user){
      return res.status(404).json({error: 'No User'});
    }
      console.log(user)
      return res.status(200).json(user);
  }).catch(function(err){
    return res.status(404).json({error:'error'})
  })
};

//인증공부
exports.auth_home = async (req , res) => {
  let user
  try {
    user = auth.verify(req.headers.authorization)
  } catch (e) {

  }

  console.log(user)
  user = user ? await models.User.findOne({where: user}) : null
  const name = user ? user.user_id : 'World'

  res.json({greeting: `Hello ${name}`})
}

exports.auth_me = async (req, res) => {
  const user = await models.User.findOne(req.user.id)
  // const accessLog = await db.findAccessLog({userId: user.id})
  res.json(user)
}

exports.login = async (req, res) => {
  const user_id = req.body.userId
  const pwd = req.body.pwd
  const user = await models.User.findOne({where: {user_id, pwd}})
  console.log(user)
  if (!user || !user.user_id) return res.status(401).json({error: 'Login failure'})
  // console.log(user.user_id)
  // await db.createAccessLog({userId: user.id})
  const accessToken = auth.signToken(user.user_id)
  const isAdmin = user.user_id === 'admin' ? true : false
  // console.log(isAdmin)
  res.status(200).json({user, accessToken, isAdmin})

}
