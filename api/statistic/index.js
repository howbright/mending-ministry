const express = require('express');
const router = express.Router();
const controller = require('./statistic.controller');

router.get('/', controller.statistic)
router.get('/addfavorite', controller.addFavorite)

module.exports = router;