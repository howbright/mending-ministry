const models = require('../../models')
const auth = require('../../auth')

exports.statistic = (req, res) => {
  console.log('ㅇㅇㅇ')
  return models.Statistic.findOne({
    where: {
      id: 0
    }
  }).then(statistic => {
    res.status(200).json(statistic)
  }).catch(function(err){
      return res.status(404).json({error:'에러~~'})
    })
}

exports.addFavorite = (req, res) => {
  models.Statistic.findById(0).then(statistic => {
    return statistic.increment('favorite', {by: 1})
  }).then(statistic => {
  	return res.status(200).json(statistic);
  // Postgres will return the updated user by default (unless disabled by setting { returning: false })
  // In other dialects, you'll want to call user.reload() to get the updated instance...
  });
}