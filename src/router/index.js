import Vue from 'vue'
import VueRouter from 'vue-router'
import VueHead from 'vue-head'
import SocialSharing from 'vue-social-sharing'
import routes from './routes'

Vue.use(VueHead)
Vue.use(VueRouter)
Vue.use(SocialSharing)

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
   *
   * If you decide to go with "history" mode, please also set "build.publicPath"
   * to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  // scrollBehavior: () => ({ y: 0 }),
  scrollBehavior: () => (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes
})

export default Router
