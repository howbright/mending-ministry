
import store from '../store'
const requireAuth = () => (from, to, next) => {
  // console.log('aa' + store.getters.isAuthenticated)
  if (store.getters.isAuthenticated) return next()
  next('/login')
}
export default [
  {
    path: '/',
    component: () => import('layouts/main'),
    children: [
      { path: '',
        component: () => import('pages/intro')
      },
      { path: 'calli',
        component: () => import('pages/calli')
      },
      { path: 'video',
        component: () => import('pages/video1')
      },
      { path: '/createletter/:user_id',
        component: () => import('pages/createletter'),
        beforeEnter: requireAuth()
      },
      { path: '/editletter/:letter_id/user/:user_id',
        component: () => import('pages/editletter'),
        beforeEnter: requireAuth()
      },
      { path: '/login', component: () => import('pages/login') },
      { path: '/register', component: () => import('pages/register') },
      { path: '/myletters/:id',
        component: () => import('pages/myletters'),
        beforeEnter: requireAuth()
      },
      { path: '/readletter/:letter_id', component: () => import('pages/readletter') },
      { path: '/readletter/:letter_id/user/:user_id', component: () => import('pages/readletter') },
      { path: '/adminletters',
        component: () => import('pages/adminletters'),
        beforeEnter: requireAuth()
      }
    ]
  },
  {
    path: '/writer',
    component: () => import('layouts/writermain'),
    children: [
      { path: '', component: () => import('pages/myletters') }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
