import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const resourceHost = 'http://7rainbow.org'

const enhanceAccessToeken = () => {
  const {accessToken} = localStorage
  if (!accessToken) return
  axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`
}
enhanceAccessToeken()

export default new Vuex.Store({
  state: {
    accessToken: null,
    userId: null,
    port: ''
  },
  getters: {
    isAuthenticated (state) {
      state.accessToken = state.accessToken || localStorage.accessToken
      return state.accessToken
    },
    userId (state) {
      state.userId = state.userId || localStorage.userId
      return state.userId
    }
  },
  mutations: {
    LOGIN (state, {accessToken, userId}) {
      state.accessToken = accessToken
      localStorage.accessToken = accessToken
      state.userId = userId
      localStorage.userId = userId
    },
    LOGOUT (state) {
      state.accessToken = null
      delete localStorage.accessToken

      state.userId = null
      delete localStorage.userId
    }
  },
  actions: {
    LOGIN ({commit}, {userId, pwd}) {
      return axios.post(`${resourceHost}/user/login`, {userId, pwd})
        .then(({data}) => {
          commit('LOGIN', {accessToken: data.accessToken, userId: userId})
          axios.defaults.headers.common['Authorization'] = `Bearer ${data.accessToken}`
        })
    },
    LOGOUT ({commit}) {
      axios.defaults.headers.common['Authorization'] = undefined
      commit('LOGOUT')
    }
  }
})
